# Hello World , Eu sou o Vinícius

<table>
  <a href="https://github.com/leehxd">
  <img height="180em" src="https://github-readme-stats.vercel.app/api?username=ViniciusGGabriel&show_icons=true&theme=tokyonight&include_all_commits=true&count_private=true"/>
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=ViniciusGGabriel&layout=compact&langs_count=6&theme=tokyonight"/>
    
  <img src="https://skillicons.dev/icons?i=js,ts,react,vite,sass,tailwind,bootstrap,git,github&theme=dark&perline=9" width="765" alt="Solido">
  <img src="https://skillicons.dev/icons?i=postgresql,mysql,nodejs,java,spring,docker&theme=dark&perline=9" width="765" alt="Aprendendo">
</table>

<div>
  <a href = "mailto:viniciusggabrielpl@gmail.com"><img src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white" target="_blank"></a>
  <a href="https://www.linkedin.com/in/vin%C3%ADcius-gabriel-pereira-leit%C3%A3o/" target="_blank"><img src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white" target="_blank"></a> 
</div>
